// Function able to receive data without the use of global variables or prompt()

// "name" - is a parameter
// A parameter is a variable/container that exists only in our function and is used to store information that is provided to a function when it is called/invoked
function printName(name) {
  console.log("My name is " + name);
}

// Data passed into a function invocation can be received by the function
// This is what we call an argument
printName("Jungkook");
printName("Thonie");

// Data passed into the function through funtion invocation is called arguments
// The argument is then stored within a container called a parameter
function printMyAge(age) {
  console.log("I am " + age);
}

printMyAge(25);
printMyAge();

// check divisibility reusably using a function with arguments and parameter
function checkDivisibilityBy8(num) {
  let remainder = num % 8;
  console.log("The remainder of " + num + " divided by 8 is: " + remainder);

  let isDivisibleBy8 = remainder === 0;
  console.log("Is " + num + " divisible by 8?");
  console.log(isDivisibleBy8);
}

checkDivisibilityBy8(64);
checkDivisibilityBy8(27);
/*
	Mini-Activity

	1. Create a function which is capable to receive data as an argument:
		- This function should be able to receive the name of your favorite superhero
		- Display the name of your favorite superhero in the console
	 2. Create a function which is capable to receive a number as an argument:
	 	- This function should be able to receive a any number
	 	- Display the number and state if even number
 */

function mySuperHero(heroName) {
  console.log("My superhero is " + heroName);
}
mySuperHero("Superman");

function evenNumberValidator(number) {
  const identifier = number % 2;
  if (identifier === 0) {
    console.log(number + " is an even number");
  } else {
    console.log(number + " is not an even number");
  }
}

const checkNumber = prompt("Please input number");
evenNumberValidator(checkNumber);

function printMyFavoriteSongs(...favoriteSongs) {
  console.log("Here are my top " + favoriteSongs.length + " favorite songs");
  let x = 1;
  for (let favoriteSong of favoriteSongs) {
    console.log(x + ". " + favoriteSong);
    x++;
  }
}
printMyFavoriteSongs("Himala",
                     "214", 
                     "Ulan", 
                     "Kisapmata", 
                     "Awit ng Kabataan");

function add(num1,num2){
    return num1 + num2;
}
const sumNumber = add(2,5);
console.log(sumNumber);

function multiplyNumbers(num1, num2){
    return num1 * num2;
}
const product = multiplyNumbers(5,7);
console.log(product);

function getAreaOfCircle(radius){
    const area =  3.1416*radius**2;
    return area;
}
const circleArea = getAreaOfCircle(3);
console.log(circleArea);

/* =====================
 Assignment:
 
 Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console. */

function getTestResult(score, totalScore){
    const testScorePercentage = (score/totalScore)*100;
    const isPassed = testScorePercentage > 75;
    return isPassed;
}

const isPassingScore = getTestResult(76,100);
console.log('Pass: ' + isPassingScore);